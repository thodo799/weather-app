import React, {Component} from 'react';
import './Weather.css'
class Weather extends Component {

    render() {
        console.log(this.props.speed)
        const temperateC=Math.floor(this.props.temperature-273.15)
        var iconUrl = "http://openweathermap.org/img/wn/" + this.props.icon + ".png";
        const current = new Date();
        const date = `${current.getDate()}/${current.getMonth()+1}/${current.getFullYear()} ${current.getHours()}:${current.getMinutes()} `;
        return (
            <div className="weather-info">

                {
                    this.props.icon
                    && <p className="weather__key">
                        <p className="date-time">{date}</p>
                        <img className="weather__img" alt="icon" src={iconUrl} />


                    </p>
                }
                {
                    this.props.temperature &&
                    <p className="temperature">


                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"
                             className="feather feather-thermometer">
                            <path d="M14 14.76V3.5a2.5 2.5 0 0 0-5 0v11.26a4.5 4.5 0 1 0 5 0z"></path>
                        </svg>
                        <span className="temperature-info"> {temperateC}°C</span>
                    </p>
                }


                {
                    this.props.country && this.props.city
                    && <p className="location">
                        <span className="location-info"> {this.props.city}, {this.props.country}</span></p>
                }
                <div className="weather__info">
                    {
                        this.props.humidity &&
                        <p className="humidity">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                 stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"
                                 className="feather feather-droplet">
                                <path d="M12 2.69l5.66 5.66a8 8 0 1 1-11.31 0z"></path>
                            </svg>

                            <span className="humidity-info">  {this.props.humidity}%</span>
                        </p>
                    }
                    {
                        this.props.description && this.props.speed&&
                        <p className="condition">
                            <span className="condition-info"> <i className="fas fa-cloud"></i>{this.props.description}</span>
                            <span className="condition-info"><i className="fas fa-wind"></i> {this.props.speed} km/h</span>
                        </p>
                    }
                </div>

                {
                    this.props.error &&
                    <p className="weather__error">{this.props.error}</p>
                }

            </div>
        );
    }
}

export default Weather;