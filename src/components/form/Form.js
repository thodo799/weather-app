import React, {Component} from 'react';
import './Form.css';
import Countries from "../../Countries";
class Form extends Component {
    constructor() {
        super();
        this.state={
            suggestions:[],
            text:''


        }

    }
    onHandleChange=(e)=>{
        const value = e.target.value
        let {suggestions} = this.state;
        if(value.length > 0){
            const regex = new RegExp(`^${value}`, 'i');
            suggestions = Countries.sort().filter(v => regex.test(v))
        }
        this.setState(() => ({
            suggestions:suggestions,
            text:value
        }))
    }
    selectedText=(value)=> {
        this.setState(() => ({
            text: value,
            suggestions: [],
        }))
    }
    renderSuggestions = () => {
        let { suggestions } = this.state;
        if(suggestions.length === 0){
            return null;
        }
        return (
            <div className="list-wrapper">
                <ul className="auto-list">
                    {
                        suggestions.map((item, index) =>
                            (<li className="auto-item" key={index} onClick={() => this.selectedText(item)}>{item}</li>))
                    }
                </ul>
                <span className="length-list">Suggestions: {this.state.suggestions.length}</span>
            </div>

        );
    }

    render() {


        return (
            <div className="form-wrapper">

                <form className="form-groups "  autoComplete="off" onSubmit = {this.props.loadWeather}>
                    <input className="form-input"
                           id="query"
                           type="text"
                           name="city"
                           value={this.state.text}
                           placeholder="City..."
                           onChange={this.onHandleChange}
                    />
                    <button className="form-input">Get Weather</button>
                </form>
                {this.renderSuggestions()}
            </div>

        );
    }
}

export default Form;