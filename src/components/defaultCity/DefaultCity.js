import React, {Component} from 'react';
import '../../App.css'
class DefaultCity extends Component {
    constructor() {
        super();
        this.state = {
            temperature: undefined,
            city: undefined,
            country: undefined,
            humidity: undefined,
            description: undefined,
            icon: undefined,
            error: undefined,

        }

    }

    async componentDidMount(){

        const apiKey = 'a79e112b2349ae479beb5adf6767af49';

        try {

            const api_de = await fetch(`http://api.openweathermap.org/data/2.5/weather?lat=10.75&lon=106.67&appid=${apiKey}`);
            const res = await api_de.json();

            console.log(res)

                this.setState({
                    temperature: res.main.temp,
                    city: res.name,
                    country: res.sys.country,
                    humidity: res.main.humidity,
                    description: res.weather[0].description,
                    speed: res.wind.speed,
                    icon: res.weather[0].icon,
                    error: ""
                })


        } catch (error) {
            this.setState({
                temperature: undefined,
                city: undefined,
                country: undefined,
                humidity: undefined,
                description: undefined,
                icon: undefined,
                speed:undefined,


                error: "Can not find out this city..."
            })
        }
    }
    render() {

        const current = new Date();
        const date = `${current.getDate()}/${current.getMonth()+1}/${current.getFullYear()} ${current.getHours()}:${current.getMinutes()} `;
        const temperateC=Math.floor(this.state.temperature-273.15)
        var iconUrl = "http://openweathermap.org/img/w/" + this.state.icon + ".png";
        return (
            <div className={this.props.hide===false?"hide":""} >
                <div className="weather-info">


                    {
                        this.state.icon
                        && <p className="weather__key">
                            <p className="date-time">{date}</p>
                            <img className="weather__img" alt="icon" src={iconUrl} />

                        </p>
                    }
                    {
                        this.state.temperature &&
                        <p className="temperature">


                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                 stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"
                                 className="feather feather-thermometer">
                                <path d="M14 14.76V3.5a2.5 2.5 0 0 0-5 0v11.26a4.5 4.5 0 1 0 5 0z"></path>
                            </svg>
                            <span className="temperature-info"> {temperateC}°C</span>
                        </p>
                    }


                    {
                        this.state.country && this.state.city
                        && <p className="location">
                            <span className="location-info"> {this.state.city}, {this.state.country}</span></p>
                    }
                    <div className="weather__info">
                        {
                            this.state.humidity &&
                            <p className="humidity">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                     stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"
                                     className="feather feather-droplet">
                                    <path d="M12 2.69l5.66 5.66a8 8 0 1 1-11.31 0z"></path>
                                </svg>

                                <span className="humidity-info">  {this.state.humidity}%</span>
                            </p>
                        }
                        {
                            this.state.description && this.state.speed&&
                            <p className="condition">
                                <span className="condition-info"> <i className="fas fa-cloud"></i>{this.state.description}</span>
                                <span className="condition-info"><i className="fas fa-wind"></i> {this.state.speed} km/h</span>
                            </p>
                        }
                    </div>

                    {
                        this.props.error &&
                        <p className="weather__error">{this.props.error}</p>
                    }
                </div>

            </div>
        );
    }
}

export default DefaultCity;