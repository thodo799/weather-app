import React, {Component} from 'react';
import Form from "./components/form/Form";
import Weather from "./components/weather/Weather";
import CurrentCity from "./components/currentCity/CurrentCity";

import DefaultCity from "./components/defaultCity/DefaultCity";
import Title from 'react-vanilla-tilt'
import CurrentCyty2 from "./components/currentCity/currentCyty2";
const apiKey = 'a79e112b2349ae479beb5adf6767af49';

class App extends Component {
    constructor() {
        super();
        this.state = {
            temperature: undefined,
            city: undefined,
            country: undefined,
            humidity: undefined,
            description: undefined,
            icon: undefined,
            error: undefined,
            speed:undefined,
            status:true,
            // lat:undefined,
            // lon:undefined,


        }

    }


    getWeather = async (e) => {

        const city = e.target.elements.city.value;
        e.preventDefault();
        try {

            const api_call = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}`);
            const response = await api_call.json();

            console.log(response)
            if (city) {
                this.setState({
                    temperature: response.main.temp,
                    city: response.name,
                    country: response.sys.country,
                    humidity: response.main.humidity,
                    description: response.weather[0].description,
                    speed: response.wind.speed,
                    icon: response.weather[0].icon,
                    error: "",
                    status:false
                })
            } else {
                this.setState({
                    error: "Please fill all fields..."
                })
            }
        } catch (error) {
            this.setState({
                temperature: undefined,
                city: undefined,
                country: undefined,
                humidity: undefined,
                description: undefined,
                icon: undefined,
                speed:undefined,

                error: "Can not find out this city..."
            })
        }
    }


    render() {
        console.log(this.state)
        return (
            <div className="app-wrapper">

                <CurrentCity/>
                    <Title className="app"
                           options={{
                               max: 25,
                               speed: 400,
                               glare:true,
                               "max-glare":1

                           }}
                    >

                        <Form loadWeather={this.getWeather}/>
                        <DefaultCity hide={this.state.status}/>
                        <Weather
                            temperature={this.state.temperature}
                            city={this.state.city}
                            country={this.state.country}
                            humidity={this.state.humidity}
                            description={this.state.description}
                            icon={this.state.icon}
                            error={this.state.error}
                            speed={this.state.speed}
                        />


                    </Title>
<CurrentCyty2/>
            </div>
        );
    }
}

export default App;